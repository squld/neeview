﻿using NeeLaboratory.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NeeView
{
    public class BookPageCollection : BindableBase, IEnumerable<Page>, IDisposable
    {
        // サムネイル寿命管理
        private PageThumbnailPool _thumbnaulPool = new PageThumbnailPool();

        // ページ列
        private PageSortMode _sortMode = PageSortMode.FileName;


        public BookPageCollection(List<Page> pages, PageSortMode sortMode)
        {
            Pages = pages;
            _sortMode = sortMode;

            foreach (var page in Pages)
            {
                page.Thumbnail.Touched += Thumbnail_Touched;
            }

            Sort();
        }


        // ソートされた
        public event EventHandler PagesSorted;

        // ファイル削除された
        public event EventHandler<PageChangedEventArgs> PageRemoved;



        // この本のアーカイバ
        public ArchiveEntryCollection ArchiveEntryCollection { get; private set; }

        // メディアアーカイバ？
        public bool IsMedia => ArchiveEntryCollection?.Archiver is MediaArchiver;

        // ページマークアーカイバ？
        public bool IsPagemarkFolder => ArchiveEntryCollection?.Archiver is PagemarkArchiver;


        public List<Page> Pages { get; private set; }

        public PageSortMode SortMode
        {
            get => _sortMode;
            set => SetProperty(ref _sortMode, value);
        }


        #region IDisposable Support
        private bool _disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    this.ResetPropertyChanged();
                    this.PageRemoved = null;
                    this.PagesSorted = null;

                    if (Pages != null)
                    {
                        Pages.ForEach(e => e?.Dispose());
                    }
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

        #region IEnumerable<Page> Support

        public IEnumerator<Page> GetEnumerator()
        {
            return Pages.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region List like

        public Page this[int index]
        {
            get { return Pages[index]; }
            set { Pages[index] = value; }
        }

        public int Count => Pages.Count;

        public int IndexOf(Page page) => Pages.IndexOf(page);

        public Page First() => Pages.First();

        public Page Last() => Pages.Last();

        #endregion

        /// <summary>
        /// サムネイル参照イベント処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Thumbnail_Touched(object sender, EventArgs e)
        {
            var thumb = (Thumbnail)sender;
            _thumbnaulPool.Add(thumb);
        }

        // ページ
        public Page GetPage(int index) => Pages.Count > 0 ? Pages[ClampPageNumber(index)] : null;

        //
        public Page GetPage(string name) => Pages.FirstOrDefault(e => e.EntryFullName == name);

        // ページ番号
        public int GetIndex(Page page) => Pages.IndexOf(page);

        // 先頭ページの場所
        public PagePosition FirstPosition() => PagePosition.Zero;

        // 最終ページの場所
        public PagePosition LastPosition() => Pages.Count > 0 ? new PagePosition(Pages.Count - 1, 1) : FirstPosition();

        // ページ番号のクランプ
        public int ClampPageNumber(int index)
        {
            if (index > Pages.Count - 1) index = Pages.Count - 1;
            if (index < 0) index = 0;
            return index;
        }

        // ページ場所の有効判定
        public bool IsValidPosition(PagePosition position)
        {
            return (FirstPosition() <= position && position <= LastPosition());
        }


        #region ページの並び替え

        // ページの並び替え
        public void Sort()
        {
            if (Pages.Count <= 0) return;

            var isSortFileFirst = BookProfile.Current.IsSortFileFirst;
            var pages = Pages.OrderBy(e => e.PageType);

            switch (SortMode)
            {
                case PageSortMode.FileName:
                    pages = pages.ThenBy(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.FileNameDescending:
                    pages = pages.ThenByDescending(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.TimeStamp:
                    pages = pages.ThenBy(e => e.Entry.LastWriteTime).ThenBy(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.TimeStampDescending:
                    pages = pages.ThenByDescending(e => e.Entry.LastWriteTime).ThenBy(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.Size:
                    pages = pages.ThenBy(e => e.Entry.Length).ThenBy(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.SizeDescending:
                    pages = pages.ThenByDescending(e => e.Entry.Length).ThenBy(e => e, new ComparerFileName(isSortFileFirst));
                    break;
                case PageSortMode.Random:
                    var random = new Random();
                    pages = pages.ThenBy(e => random.Next());
                    break;
                default:
                    throw new NotImplementedException();
            }

            Pages = pages.ToList();

            // ページ ナンバリング
            PagesNumbering();

            PagesSorted?.Invoke(this, null);
        }

        /// <summary>
        /// ページ番号設定
        /// </summary>
        private void PagesNumbering()
        {
            for (int i = 0; i < Pages.Count; ++i) Pages[i].Index = i;
        }

        /// <summary>
        /// ファイル名ソート用比較クラス
        /// </summary>
        private class ComparerFileName : IComparer<Page>
        {
            private int _sortFileFirstSign;

            public ComparerFileName(bool isSortFileFirst)
            {
                _sortFileFirstSign = isSortFileFirst ? 1 : -1;
            }

            public int Compare(Page x, Page y)
            {
                var xName = x.GetEntryFullNameTokens();
                var yName = y.GetEntryFullNameTokens();

                var limit = Math.Min(xName.Length, yName.Length);
                for (int i = 0; i < limit; ++i)
                {
                    if (xName[i] != yName[i])
                    {
                        var xIsDirectory = i + 1 == xName.Length ? x.Entry.IsDirectory : true;
                        var yIsDirectory = i + 1 == yName.Length ? y.Entry.IsDirectory : true;
                        if (xIsDirectory != yIsDirectory)
                        {
                            return (xIsDirectory ? 1 : -1) * _sortFileFirstSign;
                        }
                        return NativeMethods.StrCmpLogicalW(xName[i], yName[i]);
                    }
                }

                return xName.Length - yName.Length;
            }
        }

        #endregion

        #region ページの削除

        // ページの削除
        public void Remove(Page page)
        {
            if (Pages.Count <= 0) return;

            int index = Pages.IndexOf(page);
            if (index < 0) return;

            Pages.RemoveAt(index);

            PagesNumbering();

            PageRemoved?.Invoke(this, new PageChangedEventArgs(page));
        }

        #endregion

        #region ページリスト用現在ページ表示フラグ

        // 表示中ページ
        private List<Page> _viewPages = new List<Page>();

        /// <summary>
        /// 表示中ページフラグ更新
        /// </summary>
        public void SetViewPageFlag(List<Page> viewPages)
        {
            var hidePages = _viewPages.Where(e => !viewPages.Contains(e));

            foreach (var page in viewPages)
            {
                page.IsVisibled = true;
            }

            foreach (var page in hidePages)
            {
                page.IsVisibled = false;
            }

            _viewPages = viewPages.ToList();
        }

        #endregion

        #region フォルダーの先頭ページを取得

        public int GetNextFolderIndex(int start)
        {
            if (Pages.Count == 0 || !SortMode.IsFileNameCategory() || start < 0)
            {
                return -1;
            }

            string currentFolder = LoosePath.GetDirectoryName(Pages[start].EntryFullName);

            for (int index = start + 1; index < Pages.Count; ++index)
            {
                var folder = LoosePath.GetDirectoryName(Pages[index].EntryFullName);
                if (currentFolder != folder)
                {
                    return index;
                }
            }

            return -1;
        }

        public int GetPrevFolderIndex(int start)
        {
            if (Pages.Count == 0 || !SortMode.IsFileNameCategory() || start < 0)
            {
                return -1;
            }

            if (start == 0)
            {
                return -1;
            }

            string currentFolder = LoosePath.GetDirectoryName(Pages[start - 1].EntryFullName);

            for (int index = start - 1; index > 0; --index)
            {
                var folder = LoosePath.GetDirectoryName(Pages[index - 1].EntryFullName);
                if (currentFolder != folder)
                {
                    return index;
                }
            }

            return 0;
        }

        #endregion
    }
}
