## ChangeLog

### 35.1
(2019-07-20)

#### Bug fixes

* Fixed a bug that there may be gaps when maximizing windows.
* Fixed a bug that page marks are not reflected well at multiple startup.

### 35.0
(2019-07-13)

#### Standard support for Susie plugins (.spi)

* The standard NeeView now supports Susie plugins (.spi). It also works on 64bit OS. NeeViewS was abolished along with this.
* Support for the .sph plugin has been discontinued.

#### Bug fixes

* Fixed a bug that can not be read when the folder name is full-width space.
* Fixed a bug related to full screen state restoration.

#### Other

* Added "Allow enlarge" and "Allow reduce" settings separately from the display scale. Along with this, the setting "Fit to window if large(small)" is abolished.
* Cancel slide show timer by key operation.
* Added the option to select a banner in the form of a side panel list icon.
* Make the previous installation location the default location for upgrade installation. It works from the next update.
* Added "After deleting the vieweing book, open the next book" setting to Bookshelf settings.
* Implemented left / right switch setting in command unit to command parameter.
* Added file preference in page order to Page settings.
* Merged subfolder pages of settings into page settings.

----

Please see [here](https://bitbucket.org/neelabo/neeview/wiki/ChangeLog) for the previous change log.
