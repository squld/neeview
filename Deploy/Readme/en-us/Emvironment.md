## Environment

  * Windows 7 SP1, Windows 8.1, Windows 10
  * .NET Framework 4.7.2 or later is required. If you do not start it please get it from [Microsoft .NET](https://dotnet.microsoft.com/download/dotnet-framework-runtime) and install it.

## How to install / uninstall

### Zip version

  * NeeView<VERSION/>.zip
  
  Installation is unnecessary. After unpacking the zip, just run `NeeView.exe`.
  User data such as setting files etc. are also saved in the same place.  

  Uninstalling just deletes the file. Registry is not used.

### Installer version

  * NeeView<VERSION/>.msi

  Installation starts when you run it. Follow the instructions of the installer.  
  Configuration files etc. User data is saved in the application data folder of each user.  
  You can check this folder by "Option" > "Open setting folder" in NeeView menu.  
  
  For uninstallation, use "Apps and Features" of OS.  
  However, user data such as setting data does not disappear by just uninstalling.
  Please manually erase or execute "Delete data" (installed version only function) of NeeView setting before uninstallation.
