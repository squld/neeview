## Third-Party Licenses

### 7-Zip

DLL: 7z.dll

* Copyright (C) 1999-2018 Igor Pavlov.
* URL: [7-Zip](http://www.7-zip.org/)
* License: [GNU LGPL + BSD 3-clause License + unRAR restriction](http://www.7-zip.org/license.txt)

### SevenZipSharp

DLL: SevenZipSharpNE.dll

* Copyright (C) Markovtsev Vadim 2009, 2010
* URL: [SevenZipSharp - Home](https://sevenzipsharp.codeplex.com/)
* License: [GNU LGPL](https://sevenzipsharp.codeplex.com/license)
* Changed:
    * VS 2015 project: [Project Page](https://github.com/tomap/SevenZipSharp)
    * RAR5 Support: [Project Page](https://github.com/neelabo/SevenZipSharp)

### Material design icons

ICON: ic_help_24px, etc...

* Google (C)
* URL: [Icons - Style - Google design guidelines](http://www.google.com/design/spec/style/icons.html#icons-system-icons)
* License: [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)

### WPFDragAndDropSample

Code-in:

* Copyright (c) 2016 takanemu
* URL: [WPFDragAndDropSample](https://github.com/takanemu/WPFDragAndDropSample)
* License: [MIT License](https://github.com/takanemu/WPFDragAndDropSample/blob/master/LICENSE)

### PDFium

DLL: pdfium.dll

* Copyright 2014 PDFium Authors. All rights reserved.
* URL: [PDFium](https://pdfium.googlesource.com/pdfium/)
* License: [3-Clause BSD License](https://pdfium.googlesource.com/pdfium/+/master/LICENSE)

### PdfiumViewer

DLL: PdfiumViewer.dll

* Copyright (c) Pieter van Ginkel
* URL: [PdfiumViewer](https://github.com/pvginkel/PdfiumViewer)
* License: [Apache License 2.0](https://github.com/pvginkel/PdfiumViewer/blob/master/LICENSE)

### MagicScaler

DLL: PhotoSauce.MagicScaler.dll

* Copyright (c) 2015-2019 Clinton Ingram
* URL: [PhotoSauce](http://photosauce.net/)
* License: [MS-PL](https://github.com/saucecontrol/PhotoSauce/blob/master/license)

### .NET corefx

DLL: System.Buffers.dll, etc...

* Copyright (c) .NET Foundation and Contributors
* URL: [.NET](https://dot.net)
* License: [MIT License](https://github.com/dotnet/corefx/blob/master/LICENSE.TXT)

### Icon Fonts

ICON: pushpin, etc...

* OnlineWebFonts.COM (C)
* URL: [Icon Fonts](http://www.onlinewebfonts.com/icon)
* License: [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

### VirtualizingStackPanel

* URL: [uhimaniavwp](https://archive.codeplex.com/?p=uhimaniavwp)
* License: [MS-PL](https://opensource.org/licenses/MS-PL)

### SharpVectors

DLL: SharpVectors.*.dll

* Copyright (c) 2010 - 2018, Elinam LLC All rights reserved.
* URL: [SharpVectors](https://github.com/ElinamLLC/SharpVectors)
* License: [3-Clause BSD License](https://github.com/ElinamLLC/SharpVectors/blob/master/License.md)

### ProcessJobObject

Code-in:

* Copyright (c) Microsoft Corporation. All rights reserved.
* URL: [VSTest](https://github.com/microsoft/vstest)
* License: [MIT License](https://github.com/microsoft/vstest/blob/master/LICENSE)
